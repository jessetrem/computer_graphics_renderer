# Open GL heightmap renderer
### Author
- Jesse Tremblay 40027271

### Key bindings
- WASD: camera movement
- arrow keys: camera tilt & rotate
- Z,X: rotate model
- F, L, P: toggle render mode [F]ace, [L]ine, [P]oint
- mouse click: toggle zoom mode with mouse movement

### dependencies
- GLEW
- GLFW
- glm
- cimg

### compile
make -f makefile.mk assignment2.exe

### Credits
- openGL class abstractions inspired by [The Cherno](https://www.youtube.com/watch?v=W3gAzLwfIP0&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2)
