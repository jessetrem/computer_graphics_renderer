#version 400 core

layout (location = 0) in vec3 vert;
layout (location = 1) in vec3 i_color; // also normal

uniform mat4 cam;
// uniform mat4 world;

uniform float range;

out float depth_color;

void main() {
  vec4 newPos = cam * vec4(vert, 1.0);
  gl_Position = newPos;
  depth_color = newPos.z / range;
}

~~pixel_boi~~
#version 400 core

in float depth_color;

layout(location = 0) out vec4 color;

void main() {
  color = vec4(depth_color, depth_color, depth_color, 1.0f);
}