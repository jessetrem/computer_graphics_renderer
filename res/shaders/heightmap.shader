#version 400 core

struct PointLight
{
  vec3 position;
  vec3 color;
  float intensity;
};

layout (location = 0) in vec3 vert;
layout (location = 1) in vec3 i_color; // also normal

uniform mat4 cam;
uniform mat4 world;
uniform mat4 light_view;
uniform PointLight light;



out vec3 o_color;
out vec3 l_color;
out float l_intensity;
out vec3 surface_normal;
out vec3 to_light;
out vec4 world_position;
out vec4 shadow_coord;

// uniform mat4 mvp;


void main() {
  world_position = world * vec4(vert, 1.0);
  gl_Position = cam * world * vec4(vert, 1.0);
  o_color = i_color;
  l_color = light.color;
  l_intensity = light.intensity;
  surface_normal = (world * vec4(i_color, 0.0)).xyz;
  to_light = light.position - world_position.xyz;
  shadow_coord = light_view * vec4(vert, 1.0);
}

~~pixel_boi~~
#version 400 core

vec4 compute_specular(vec3 vertex_normal, vec3 vertex_ligth, vec3 frag_position, vec4 light_color);
vec4 compute_diffuse(vec3 vertex_normal, vec3 vertex_ligth, vec4 light_color);

in vec3 o_color;
in vec3 l_color;
in float l_intensity;
in vec3 surface_normal;
in vec3 to_light;
in vec4 world_position;
in vec4 shadow_coord;

uniform vec3 cam_pos;
uniform sampler2D shadow_map;
uniform float bias;
uniform float range;

out vec4 color;

void main()
{
  float shadow = 1.0f;
  if (texture(shadow_map, shadow_coord.xy / shadow_coord.w).z < (shadow_coord.z / range) - bias) {
    shadow = 0.0f;
  }

  float falloff = max((range - abs(length(to_light))) / range, 0);
  
  vec4 light_col = vec4(l_color, 1.0f);
  vec3 vertex_normal = normalize(surface_normal);
  vec3 vertex_ligth = normalize(to_light);
  vec4 ambient = vec4(light_col.xyz * 0.1, 1.0f);
  vec4 diffuse = compute_diffuse(vertex_normal, vertex_ligth, light_col);
  vec4 specular = compute_specular(vertex_normal, vertex_ligth, world_position.xyz, light_col);
  
  color = (ambient + (shadow * (diffuse + specular) * falloff)) * vec4(1.0f, 1.0f, 0.0f, 1.0f);
}

vec4 compute_specular(vec3 vertex_normal, vec3 vertex_ligth, vec3 frag_position, vec4 light_color) {
  float specularStrength = 0.5; // make uniform
  vec3 viewDir = normalize(cam_pos - world_position.xyz);
  vec3 reflectDir = reflect(-vertex_ligth, vertex_normal); 
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), 64);
  return specularStrength * spec * light_color;
}

vec4 compute_diffuse(vec3 vertex_normal, vec3 vertex_ligth, vec4 light_color) {
  float brigthness = max(dot(vertex_normal, vertex_ligth), 0.0);
  return brigthness * light_color;
}
