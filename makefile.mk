# TODO clean me up
OUTDIR = out
SRC = src
INCLUDES = -I ./lib -DGLEW_STATIC
OBJS = $(OUTDIR)/vertex_buffer.o $(OUTDIR)/vertex_buffer_layout.o $(OUTDIR)/shader.o $(OUTDIR)/input_handler.o $(OUTDIR)/test_command.o $(OUTDIR)/window_commands.o $(OUTDIR)/vertex_array.o $(OUTDIR)/camera.o $(OUTDIR)/model.o $(OUTDIR)/index_buffer.o $(OUTDIR)/heightmap.o $(OUTDIR)/cursor.o $(OUTDIR)/renderer.o $(OUTDIR)/obj_3d.o $(OUTDIR)/point_light.o
LIBS = -Lbin -lglfw3 -lglew32s -lopengl32 -lgdi32 -DGLEW_STATIC
CC = g++
OPTIMIZED = -g
CFLAGS = -c $(OPTIMIZED)
FLAGS = -std=c++11

lab1.exe: $(OBJS) $(OUTDIR)/lab1.o
	$(CC) $(OPTIMIZED) $(FLAGS) $(OBJS) $(OUTDIR)/lab1.o $(INCLUDES) $(LIBS) -o lab1.exe

assignment.exe: $(OBJS) $(OUTDIR)/assignment.o
	$(CC) $(OPTIMIZED) $(FLAGS) $(OBJS) $(OUTDIR)/assignment.o $(INCLUDES) $(LIBS) -o assignment.exe

$(OUTDIR)/assignment.o: $(SRC)/assignment.cpp
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/assignment.cpp -o $(OUTDIR)/assignment.o

$(OUTDIR)/lab1.o: $(SRC)/lab1.cpp
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/lab1.cpp -o $(OUTDIR)/lab1.o

$(OUTDIR)/vertex_buffer.o: $(SRC)/vertex_buffer.cpp $(SRC)/vertex_buffer.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/vertex_buffer.cpp -o $(OUTDIR)/vertex_buffer.o

$(OUTDIR)/vertex_buffer_layout.o: $(SRC)/vertex_buffer_layout.cpp $(SRC)/vertex_buffer_layout.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/vertex_buffer_layout.cpp -o $(OUTDIR)/vertex_buffer_layout.o

$(OUTDIR)/shader.o: $(SRC)/shader.cpp $(SRC)/shader.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/shader.cpp -o $(OUTDIR)/shader.o

$(OUTDIR)/index_buffer.o: $(SRC)/index_buffer.cpp $(SRC)/index_buffer.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/index_buffer.cpp -o $(OUTDIR)/index_buffer.o

$(OUTDIR)/vertex_array.o: $(SRC)/vertex_array.cpp $(SRC)/vertex_array.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/vertex_array.cpp -o $(OUTDIR)/vertex_array.o

$(OUTDIR)/model.o: $(SRC)/model.cpp $(SRC)/model.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/model.cpp -o $(OUTDIR)/model.o

$(OUTDIR)/renderer.o: $(SRC)/renderer.cpp $(SRC)/renderer.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/renderer.cpp -o $(OUTDIR)/renderer.o

$(OUTDIR)/heightmap.o: $(SRC)/heightmap.cpp $(SRC)/heightmap.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/heightmap.cpp -o $(OUTDIR)/heightmap.o

$(OUTDIR)/obj_3d.o: $(SRC)/obj_3d.cpp $(SRC)/obj_3d.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/obj_3d.cpp -o $(OUTDIR)/obj_3d.o

$(OUTDIR)/camera.o: $(SRC)/camera.cpp $(SRC)/camera.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/camera.cpp -o $(OUTDIR)/camera.o

$(OUTDIR)/point_light.o: $(SRC)/point_light.cpp $(SRC)/point_light.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/point_light.cpp -o $(OUTDIR)/point_light.o

$(OUTDIR)/input_handler.o: $(SRC)/input/input_handler.cpp $(SRC)/input/input_handler.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/input/input_handler.cpp -o $(OUTDIR)/input_handler.o

$(OUTDIR)/cursor.o: $(SRC)/input/cursor.cpp $(SRC)/input/cursor.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/input/cursor.cpp -o $(OUTDIR)/cursor.o

$(OUTDIR)/test_command.o: $(SRC)/input/test_command.cpp $(SRC)/input/test_command.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/input/test_command.cpp -o $(OUTDIR)/test_command.o

$(OUTDIR)/window_commands.o: $(SRC)/input/window_commands.cpp $(SRC)/input/window_commands.h
	$(CC) $(CFLAGS) $(FLAGS) $(INCLUDES) $(SRC)/input/window_commands.cpp -o $(OUTDIR)/window_commands.o
