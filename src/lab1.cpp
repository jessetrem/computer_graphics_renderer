#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> // does not seem to cause compile errors
#include <iostream>
#include "./vertex_buffer.h"
#include "./vertex_buffer_layout.h"

GLFWwindow * init();
void key(GLFWwindow *win, int key, int sc, int action, int mods) {
  switch (key) {
    case GLFW_KEY_ESCAPE:
      glfwSetWindowShouldClose(win, true);
      break;
  }
}

int main()
{
  GLFWwindow *window = init();
  if (!window)
  {
    std::cout << "FAIL" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwSwapInterval(1);

  glfwSetKeyCallback(window, key);

  glm::vec3 triangle[3] = {
    glm::vec3(-0.5, -0.5, 0),
    glm::vec3(0.5, -0.5, 0),
    glm::vec3(0, 0.5, 0),
  };

  BufferLayout bl;
  VertexBuffer * vbo = new VertexBuffer();
  unsigned int vao;
  
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  
  vbo->bind();
  vbo->setData(triangle, sizeof(triangle));

  bl.pushFloat(3);
  glVertexAttribPointer(0, bl[0].count,  bl[0].type, bl[0].normalized, 0, (void *)0);
  glEnableVertexAttribArray(0);

  std::cout << "array size: " << sizeof(triangle) / sizeof(glm::vec3) << std::endl;
  while (!glfwWindowShouldClose(window))
  {
    /* Render here */
    glClearColor(1, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle) / sizeof(glm::vec3));
    /* Swap front and back buffers */
    glfwSwapBuffers(window);

    /* Poll for and process events */
    glfwPollEvents();
  }

  return 0;
}

GLFWwindow *init()
{
  glfwInit();
  GLFWwindow *win = glfwCreateWindow(1280, 720, "lab 1", NULL, NULL);
  glfwMakeContextCurrent(win);

  GLenum glewErr = glewInit();
  if (GLEW_OK != glewErr)
  {
    std::cout << "FAIL GLEW" << std::endl;
    return 0;
  }

  return win;
}