#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include "./vertex_array.h"
#include "./vertex_buffer.h"
#include "./vertex_buffer_layout.h"
#include "./shader.h"
#include "./camera.h"
#include "./model.h"
#include "./input/input_handler.h"
#include "./input/test_command.h"
#include "./input/window_commands.h"
#include "./input/cursor.h"
#include "./index_buffer.h"
#include "./renderer.h"
#include "./obj_3d.h"
#include "./point_light.h"

int main();
GLFWwindow *init();
void run(GLFWwindow *win);

int main()
{
  GLFWwindow *win = init();
  GLFWmonitor* primary = glfwGetPrimaryMonitor();

  // glfwSetWindowMonitor(win, primary, 480, 270, 3840, 2160, 60);
  if (!win)
  {
    std::cout << "FAIL" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwSwapInterval(1);
  run(win);
}

void run(GLFWwindow *win)
{
  PointLight pl({2.0f, 25.0f, 0.0f}, {1.0f, 1.0f, 1.0f}, 200.0f);
  Renderer rend(&pl);
  InputHandler ih;
  InputHandler::setActive(&ih);
  Cursor cur;
  Cursor::setCursor(&cur);
  TestCommand lineCommand(GL_LINE);
  TestCommand triangleCommand(GL_FILL);
  TestCommand pointCommand(GL_POINT);
  Camera cam(glm::vec3(0.0f, 6.0f, -10.0f), 0, 0, 16.0f/9.0f, glm::radians(50.625), &cur);
  // command initialization
  CloseWindowCommand cwc(win);
  CameraMoveCommand moveLeft(glm::vec3(0.5, 0.0, 0.0), &cam);
  CameraMoveCommand moveRight(glm::vec3(-0.5, 0.0, 0.0), &cam);
  CameraMoveCommand moveForward(glm::vec3(0.0, 0.0, 0.5), &cam);
  CameraMoveCommand moveBack(glm::vec3(0.0, 0.0, -0.5), &cam);

  CameraMoveCommand stopLeft(glm::vec3(-0.5, 0.0, 0.0), &cam);
  CameraMoveCommand stopRight(glm::vec3(0.5, 0.0, 0.0), &cam);
  CameraMoveCommand stopForward(glm::vec3(0.0, 0.0, -0.5), &cam);
  CameraMoveCommand stopBack(glm::vec3(0.0, 0.0, 0.5), &cam);

  CameraRotateCommand rotateLeft(0.01, 0.0, &cam);
  CameraRotateCommand rotateRight(-0.01, 0.0, &cam);
  CameraRotateCommand tiltUp(0.0, -0.01, &cam);
  CameraRotateCommand tiltDown(0.0, 0.01, &cam);

  CameraRotateCommand stopRotateLeft(-0.01, 0.0, &cam);
  CameraRotateCommand stopRotateRight(0.01, 0.0, &cam);
  CameraRotateCommand stopTiltUp(0.0, 0.01, &cam);
  CameraRotateCommand stopTiltDown(0.0, -0.01, &cam);

  CameraZoomCommand zoom(&cam, true);
  CameraZoomCommand stopZoom(&cam, false);

  // MoveLightCommand moveLightUp(&pl, glm::vec3(0.0f, 0.5f, 0.0f));
  // MoveLightCommand moveLightDown(&pl, glm::vec3(0.0f, -0.5f, 0.0f));

  ChangeLightBiasCommand biasPlus(&pl, 0.001f);
  ChangeLightBiasCommand biasMinus(&pl, -0.001f);

  MoveLightCommand moveLightForward(&pl, glm::vec3(0.0f, 0.0f, 0.5f));
  MoveLightCommand moveLightBackward(&pl, glm::vec3(0.0f, 0.0f, -0.5f));
  MoveLightCommand moveLightLeft(&pl, glm::vec3(0.5f, 0.0f, 0.0f));
  MoveLightCommand moveLightRight(&pl, glm::vec3(-0.5f, 0.0f, 0.0f));

  // bind input handler to kb callback
  glfwSetKeyCallback(win, [](GLFWwindow* window, int key, int scancode, int action, int mods)  {
    InputHandler *ih_carried = InputHandler::getActive();
    ih_carried->process(key, scancode, action, mods);
  });
  // bind input handler to mouse click callback
  glfwSetMouseButtonCallback(win, [](GLFWwindow* window, int key, int action, int mods)  {
    InputHandler *ih_carried = InputHandler::getActive();
    ih_carried->process(key, -1, action, mods);
  });
  // bind cursor object to cursor movement
  glfwSetCursorPosCallback(win, [](GLFWwindow* window, double xpos, double ypos) {
    Cursor *c = Cursor::getCursor();
    c->update(xpos, ypos);
  });
  // glPolygon mode commands
  ih.registerKey(GLFW_KEY_G, GLFW_PRESS, &lineCommand);
  ih.registerKey(GLFW_KEY_F, GLFW_PRESS, &triangleCommand);
  ih.registerKey(GLFW_KEY_P, GLFW_PRESS, &pointCommand);
  ih.registerKey(GLFW_KEY_ESCAPE, GLFW_RELEASE, &cwc);
  // start camera movement
  ih.registerKey(GLFW_KEY_A, GLFW_PRESS, &moveLeft);
  ih.registerKey(GLFW_KEY_D, GLFW_PRESS, &moveRight);
  ih.registerKey(GLFW_KEY_W, GLFW_PRESS, &moveForward);
  ih.registerKey(GLFW_KEY_S, GLFW_PRESS, &moveBack);
  // stop camera movement
  ih.registerKey(GLFW_KEY_A, GLFW_RELEASE, &stopLeft);
  ih.registerKey(GLFW_KEY_D, GLFW_RELEASE, &stopRight);
  ih.registerKey(GLFW_KEY_W, GLFW_RELEASE, &stopForward);
  ih.registerKey(GLFW_KEY_S, GLFW_RELEASE, &stopBack);
  // start camera movement
  ih.registerKey(GLFW_KEY_LEFT, GLFW_PRESS, &rotateLeft);
  ih.registerKey(GLFW_KEY_RIGHT, GLFW_PRESS, &rotateRight);
  ih.registerKey(GLFW_KEY_UP, GLFW_PRESS, &tiltUp);
  ih.registerKey(GLFW_KEY_DOWN, GLFW_PRESS, &tiltDown);
  // stop camera movement
  ih.registerKey(GLFW_KEY_LEFT, GLFW_RELEASE, &stopRotateLeft);
  ih.registerKey(GLFW_KEY_RIGHT, GLFW_RELEASE, &stopRotateRight);
  ih.registerKey(GLFW_KEY_UP, GLFW_RELEASE, &stopTiltUp);
  ih.registerKey(GLFW_KEY_DOWN, GLFW_RELEASE, &stopTiltDown);

  // cam zoom commands
  ih.registerKey(GLFW_MOUSE_BUTTON_1, GLFW_PRESS, &zoom);
  ih.registerKey(GLFW_MOUSE_BUTTON_1, GLFW_RELEASE, &stopZoom);

  // light controls
  ih.registerKey(GLFW_KEY_EQUAL, GLFW_PRESS, &biasPlus);
  // ih.registerKey(GLFW_KEY_EQUAL, GLFW_RELEASE, &biasMinus);
  ih.registerKey(GLFW_KEY_MINUS, GLFW_PRESS, &biasMinus);
  // ih.registerKey(GLFW_KEY_MINUS, GLFW_RELEASE, &moveLightUp);
  // forward
  ih.registerKey(GLFW_KEY_I, GLFW_PRESS, &moveLightForward);
  ih.registerKey(GLFW_KEY_I, GLFW_RELEASE, &moveLightBackward);
  // backward
  ih.registerKey(GLFW_KEY_K, GLFW_PRESS, &moveLightBackward);
  ih.registerKey(GLFW_KEY_K, GLFW_RELEASE, &moveLightForward);
  // left
  ih.registerKey(GLFW_KEY_J, GLFW_PRESS, &moveLightLeft);
  ih.registerKey(GLFW_KEY_J, GLFW_RELEASE, &moveLightRight);
  // right
  ih.registerKey(GLFW_KEY_L, GLFW_PRESS, &moveLightRight);
  ih.registerKey(GLFW_KEY_L, GLFW_RELEASE, &moveLightLeft);

  // ColoredVertex triangle[3] = {
  //     { glm::vec3(0, 0.5, 10), glm::vec3(0.0f, 1.0f, 1.0f) },
  //     { glm::vec3(0.5, -0.5, 10), glm::vec3(1.0f, 1.0f, 0.0f) },
  //     { glm::vec3(-0.5, -0.5, 10), glm::vec3(1.0f, 0.0f, 1.0f) }
  // };

  // unsigned int indicies[3] = {0, 1, 2};

  Object3D obj("./res/obj/scene.obj");

  // draw/update loop
  while (!glfwWindowShouldClose(win))
  {
    glClearColor(0.33, 0.58, 0.93, 1); // cornflower blue
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    pl.update();
    cam.update();
    // triangleModel.draw(&rend, &cam);
    // hm.draw(&rend, &cam);
    obj.draw(&rend, &cam);
    glfwSwapBuffers(win);

    /* Poll for and process events */
    glfwPollEvents();
  }
}

// initialize the glfw window
GLFWwindow *init()
{
  glfwInit();
  GLFWwindow *win = glfwCreateWindow(1920, 1080, "Assignment", NULL, NULL);
  glfwMakeContextCurrent(win);

  GLenum glewErr = glewInit();
  if (GLEW_OK != glewErr)
  {
    std::cout << "FAIL GLEW" << std::endl;
    return 0;
  }

  return win;
}
