#include <glm/glm.hpp>
#include "./input/command.h"

class PointLight {
public:
  PointLight(glm::vec3 position, glm::vec3 color, float intensity);
  ~PointLight();
  void update();
  void applyForce(glm::vec3 force);
  glm::vec3 & getPosition();
  glm::vec3 & getColor();
  float & getIntensity();
  float & getBias();
  void modifyBias(float offset);
  glm::mat4x4 getMVP();
private:
  glm::vec3 m_velocity;
  glm::vec3 m_position;
  glm::vec3 m_direction;
  glm::vec3 m_color;
  float m_intensity;
  float m_bias;
};

class MoveLightCommand : public Command {
public:
  MoveLightCommand(PointLight *pl, glm::vec3 force);
  ~MoveLightCommand();
  void execute() override;
private:
  glm::vec3 m_force;
  PointLight *m_point_light;
};

class ChangeLightBiasCommand : public Command {
public:
  ChangeLightBiasCommand(PointLight *pl, float amount);
  ~ChangeLightBiasCommand();
  void execute() override;
private:
  float m_amount;
  PointLight *m_point_light;
};