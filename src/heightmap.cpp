#include <cimg/CImg.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <iostream>
#include "./heightmap.h"
#include "./camera.h"
#include "./input/input_handler.h"
#include "./input/command.h"
#include "./renderer.h"

HeightMap::HeightMap(std::string heightmapName) {
  parseImage(heightmapName);
}

HeightMap::~HeightMap() {
  delete model;
  // delete[] model_data;
  // delete[] model_index;
}

void HeightMap::update() {
  model->update(); // move me out of here
}

void HeightMap::draw(Renderer *r, Camera *cam) {
  model->draw(r, cam);
}


void HeightMap::parseImage(std::string img) {
  cimg_library::CImg<unsigned char> image(img.c_str());
  width = (int)image.width();
  height = (int)image.height();
  size = height * width;
  model_data = new ColoredVertex[size];
  for (int i = 0; i < size; ++i) {
    int y = i / width;
    int x = i - (width * y);
    unsigned char colorR = *image.data(x, y, 0, 0);
    // subtract w/2 from x and y to center the mesh at the origin of model space
    model_data[i] = {glm::vec3(x - (width/2), (float)colorR / 5, y - (height/2)), glm::vec3((float)colorR / 255.0f, (float)colorR / 255.0f, (float)colorR / 255.0f)};
  }
  index_size = 6 * ((width - 1) * (height-1)); // number of squares * number of indicies per square
  model_index = new unsigned int[index_size];
  for (int index_inc = 0; index_inc < index_size; index_inc += 6) {
    int y = (index_inc / 6) / (width - 1);
    int x = (index_inc / 6) - ((width - 1) * y);
    model_index[index_inc] = x + (y * width);
    model_index[index_inc + 1] = (x + (y * width)) + (width);// x + (y * width) + 1;
    model_index[index_inc + 2] = x + (y * width) + 1;// (x + (y * width)) + (width);
    model_index[index_inc + 3] = x + 1 + (y * width);
    model_index[index_inc + 4] = x + (y * width) + (width);// (x + 1 + (y * width)) + (width);
    model_index[index_inc + 5] = (x + 1 + (y * width)) + (width);// x + (y * width) + (width);
    // if (x == 0) {
    //   std::cout << "\n";
    // }
    // std::cout << model_index[index_inc] << " " << model_index[index_inc + 1]<< " " << model_index[index_inc + 2] << " " << model_index[index_inc + 3] << " " << model_index[index_inc + 4] << " " << model_index[index_inc + 5] << " ";
  }
  // std::cout << std::endl;
  model = new Model(size, model_data, index_size, model_index);
}

void HeightMap::registerCommands(InputHandler *ih) {
  left_rot = new ModelRotateCommand(model, 0.05);
  right_rot = new ModelRotateCommand(model, -0.05);
  stop_left_rot = new ModelRotateCommand(model, -0.05);
  stop_right_rot = new ModelRotateCommand(model, 0.05);

  ih->registerKey(GLFW_KEY_Z, GLFW_PRESS, left_rot);
  ih->registerKey(GLFW_KEY_X, GLFW_PRESS, right_rot);
  ih->registerKey(GLFW_KEY_Z, GLFW_RELEASE, stop_left_rot);
  ih->registerKey(GLFW_KEY_X, GLFW_RELEASE, stop_right_rot);
}
