#pragma once
#include <glm/glm.hpp>
#include "./input/command.h"

class VertexBuffer;
class VertexArray;
class BufferLayout;
class IndexBuffer;
class Shader;
class Camera;
class Renderer;
/**
 * @brief The vertex type used by the model
 * 
 */
struct ColoredVertex {
  glm::vec3 position; // position info
  glm::vec3 color; // color info
};

/**
 * @brief A 3d model composed of a VAO, VBO, Index buffer and shader that describe all that is
 * required to draw it using open gl
 * 
 */
class Model 
{
public:
  Model(unsigned int size, ColoredVertex *data, unsigned int indexSize, unsigned int *indicies);
  ~Model();
  /**
   * @brief Method to rotate the model around the Y axis
   * 
   * @param velocity the rotation velocity
   */
  void rotateY(float velocity);
  /**
   * @brief method to update the models rotation with its rotation velocity
   * 
   */
  void update();
  /**
   * @brief Method to draw the model
   * 
   * @param cam 
   */
  void draw(Renderer *r, Camera *cam);
private:
  unsigned int size;
  ColoredVertex *data;
  unsigned int indexSize;
  unsigned int *indicies;
  Shader *shader;
  Shader *shader2;
  VertexArray *vao;
  VertexBuffer *vbo;
  BufferLayout *bl;
  IndexBuffer *ib;

  float rotation_y_velocity;
  float rotation_y;
};

/**
 * @brief Command to rotate the model around the Y axis
 * 
 */
class ModelRotateCommand : public Command {
public:
  ModelRotateCommand(Model *target, float velocity);
  ~ModelRotateCommand();
  void execute();
private:
  Model *target;
  float velocity;
};

