#include <vector>
#include <iostream>
#include <unordered_map>
#include <glm/glm.hpp>
#include "./model.h"
#include "./obj_3d.h"

Object3D::Object3D(std::string fileName) {
  std::ifstream file(fileName);
  parseObj(file);
}

Object3D::~Object3D() {
  delete model;
}

void Object3D::draw(Renderer *r, Camera *cam) {
  model->draw(r, cam);
}

void Object3D::parseObj(std::ifstream &file) {
  unsigned int vertCount = 0;
  unsigned int faceCount = 0;
  
  float x, y, z;
  char f1[25], f2[25], f3[25];
  
  char type[2];
  std::string line;

  std::vector<glm::vec3> verts;
  std::vector<glm::vec3> normals;
  std::vector<std::string> faceDefs;

  
  while(std::getline(file, line)) {
    // TODO add texture fetching
    if (line[0] == 'v') {
      sscanf(line.c_str(), "%s %f %f %f", &type, &x, &y, &z);
      if (std::string(type) == "v") {
        verts.push_back({x, y, z});
      } else if (std::string(type) == "vn") {
        normals.push_back({x, y, z});
      }
    } else if (line[0] == 'f') {
      sscanf(line.c_str(), "%s %s %s %s", type, f1, f2, f3);
      ++faceCount;
      faceDefs.push_back(f1);
      faceDefs.push_back(f2);
      faceDefs.push_back(f3);
    }
  }
  unsigned int indexSize = faceCount  * 3;
  unsigned int *indicies = new unsigned int[indexSize];

  unsigned int indexCount = 0;
  unsigned int uniqueCount = 0;

  std::unordered_map<std::string, unsigned int> indexMap;

  for (std::vector<std::string>::iterator i = faceDefs.begin(); i != faceDefs.end(); ++i) {
    if (indexMap.find(*i) == indexMap.end()) {
      indicies[indexCount] = uniqueCount;
      indexMap[*i] = uniqueCount;
      ++uniqueCount;
    } else {
      indicies[indexCount] = indexMap[*i];
    }
    ++indexCount;
  }

  unsigned int vertexType;

  unsigned int position = 0;
  unsigned int currIndex = 0;

  std::string numBuffer;
  ColoredVertex *cv = new ColoredVertex[uniqueCount];
  unsigned int vertIndex, normIndex, texIndex;
  unsigned int *vertexTypeMap[3] = { &vertIndex, &texIndex,  &normIndex };

  // parse face data from obj file
  for (std::vector<std::string>::iterator i = faceDefs.begin(); i != faceDefs.end(); ++i) {
    vertexType = 0;
    numBuffer = "";
    currIndex = indexMap[*i];
    if (currIndex <= position) {
      for (std::string::iterator character = i->begin(); character != i->end(); ++character) {
        if (*character == '/') {
          if (numBuffer != "") {
            *vertexTypeMap[vertexType] = std::stof(numBuffer);
            numBuffer = "";
          } else {
            *vertexTypeMap[vertexType] = 0;
          }
          ++vertexType;
        } else {
          numBuffer += *character;
        }
      }
      // dump normal data (ternery should protect from crashing if no normal data us present)
      *vertexTypeMap[vertexType] = numBuffer != "" ? std::stof(numBuffer) : 0;
      cv[currIndex] = {verts[vertIndex - 1], normals[normIndex - 1]};
    }
    ++position;
  }

  model = new Model(uniqueCount, cv, indexSize, indicies);
}
