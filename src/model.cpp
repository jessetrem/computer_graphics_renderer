#define GLM_ENABLE_EXPERIMENTAL
#include <GLEW/glew.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include "./camera.h"
#include "./model.h"
#include "./vertex_array.h"
#include "./vertex_buffer.h"
#include "./vertex_buffer_layout.h"
#include "./shader.h"
#include "./index_buffer.h"
#include "./input/command.h"
#include "./renderer.h"
// TODO model should probably be responsible for its own data

Model::Model(unsigned int size, ColoredVertex *data, unsigned int indexSize, unsigned int *indicies): size(size), data(data), indexSize(indexSize), indicies(indicies), rotation_y(0), rotation_y_velocity(0) {
  vao = new VertexArray();
  shader = new Shader("./res/shaders/heightmap.shader");
  // shader2 = new Shader("./res/shaders/shadow.shader");
  bl = new BufferLayout(2);
  bl->pushFloat(3);
  bl->pushFloat(3);
  vbo = new VertexBuffer();
  ib = new IndexBuffer();
  ib->setData(indicies, indexSize * sizeof(unsigned int));
  vbo->setData(data, size * sizeof(ColoredVertex));
  vao->setData(*vbo, *bl, *ib);
}

Model::~Model() {
  delete vbo;
  delete bl;
  delete vao;
  delete[] indicies;
  delete[] data;
}

void Model::rotateY(float velocity) {
  rotation_y_velocity += velocity;
}

void Model::update() {
  rotation_y += rotation_y_velocity;
}

// TODO move drawing logic out of object but for now just pass light to object its the only way :(
void Model::draw(Renderer *r, Camera *cam) {
  // std::cout << rotation_y << ":" << rotation_y_velocity << std::endl;
  glm::mat4x4 rot = glm::rotate(rotation_y, glm::vec3(0,1,0));
  glm::mat4x4 camMatrix = cam->getMVP();
  shader->setUniformFm("cam", camMatrix);
  shader->setUniformFm("world", rot);
  float range = 1000.0f;
  shader->setUniformF("range", range);
  shader->setUniformFv("cam_pos", cam->getPosition());
  r->draw(vao, shader, ib);
}

ModelRotateCommand::ModelRotateCommand(Model *target, float velocity): target(target), velocity(velocity) {}
ModelRotateCommand::~ModelRotateCommand() {}
void ModelRotateCommand::execute() {
  target->rotateY(velocity);
}