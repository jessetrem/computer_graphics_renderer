#include <iostream>
#include <GLEW/glew.h>
#include "./test_command.h"

TestCommand::TestCommand(unsigned int renderMode): render_mode(renderMode) {}
TestCommand::~TestCommand() {}
void TestCommand::execute()
{
    glPolygonMode(GL_FRONT_AND_BACK, render_mode);
}