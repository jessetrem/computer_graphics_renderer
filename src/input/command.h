#pragma once
/**
 * @brief Class to be execited on an event
 * 
 */
class Command
{
public:
  // TODO commands should not have input information seperation should be handled by the input handler
  virtual void execute() = 0;
};