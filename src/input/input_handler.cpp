#include <iostream>
#include "./input_handler.h"
#include "./command.h"

InputHandler::InputHandler() {}
InputHandler::~InputHandler()
{
  std::unordered_map<unsigned int, CommandChain *>::iterator i;
  for (int actionIndex = 0; actionIndex < 3; ++actionIndex) {
    i = keyboard[actionIndex].begin();
    while (i != keyboard[actionIndex].end())
    {
      delete i->second;
      i++;
    }

    keyboard[actionIndex].clear();
  }
}

void InputHandler::process( int key, int sc, int action, int mods)
{
  CommandChain *next = keyboard[action][key];
  if (next)
  {
    do
    {
      next->current->execute();
    } while (next = next->next);
  }
}

CommandChain * InputHandler::registerKey(int keyCode, int action, Command *command)
{
  CommandChain *target = new CommandChain({command, nullptr});
  CommandChain *current = keyboard[action][keyCode];
  if (current)
  {
    do 
    {
      if (!current->next) {
        current->next = target;
        return target;
      }
    } while (current = current->next);
  } else {
    keyboard[action][keyCode] = target;
  }
  return target;
}

void InputHandler::setActive(InputHandler *ih) {
  active_input_handler = ih;
}

InputHandler * InputHandler::getActive() {
  return active_input_handler;
}

InputHandler * InputHandler::active_input_handler = nullptr;
