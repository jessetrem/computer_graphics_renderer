#pragma once
#include "./command.h"

/**
 * @brief commnad used to swap render modes
 * 
 */
class TestCommand : public Command {
public:
  TestCommand(unsigned int renderMode);
  ~TestCommand();
  virtual void execute() override;
private:
  unsigned int render_mode;
};