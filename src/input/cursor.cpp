#include "./cursor.h"


Cursor::Cursor(): x(0), y(0), delta_x(0), delta_y(0) {}
Cursor::~Cursor() {}

Cursor * Cursor::getCursor() {
  return cur;
}

void Cursor::setCursor(Cursor *c) {
  cur = c;
}

void Cursor::update(float newX, float newY) {
  delta_x = newX - x;
  delta_y = newY - y;
  x = newX;
  y = newY;
}

float Cursor::getDeltaX() {
  int currDelta = delta_x;
  delta_x = 0; // flush delta so that nex fetch does not require mouse movement to be relavent
  return currDelta;
}

float Cursor::getDeltaY() {
  int currDelta = delta_y;
  delta_y = 0; // flush delta so that nex fetch does not require mouse movement to be relavent
  return currDelta;
}

Cursor * Cursor::cur = nullptr;
