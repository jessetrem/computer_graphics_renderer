#pragma once
/**
 * @brief Object oriented abstraction of opengl vertex buffer object
 * 
 */
class VertexBuffer {
  public:
    VertexBuffer();
    ~VertexBuffer();
    void setData(void *data, unsigned int size);
    void bind();
    void unbind();
  private:
    /**
     * @brief Bind a buffer without overwriting the current_buffer
     * 
     */
    void stateless_bind();
    unsigned int vbo;
    static VertexBuffer * current_buffer;
};