#include <string>
#include <fstream>

class Model;
class Renderer;
class Camera;

class Object3D
{
public:
  Object3D(std::string objFileName);
  ~Object3D();
  void draw(Renderer *r, Camera *cam);
private:
  void parseObj(std::ifstream &file);
  Model *model;
};