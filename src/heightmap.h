#pragma once
#include <iostream>
#include "./model.h"

class Camera;
class InputHandler;
class Renderer;

/**
 * @brief class that represents a heightmap model loaded from an image file
 * 
 * @note only supports bitmaps at the moment as cimg requires additional libraries for other file formats 
 */
class HeightMap {
public:
  HeightMap(std::string heightmapName);
  ~HeightMap();
  /**
   * @brief method to register all model related commands for the heightmap
   * 
   * @param ih 
   */
  void registerCommands(InputHandler *ih);
  /**
   * @brief function that updates the model
   * 
   */
  void update();
  /**
   * @brief function that draws the model
   * 
   * @param cam the camera
   */
  void draw(Renderer *r, Camera *cam);
private:
  void parseImage(std::string heightmapName);
  Model *model;
  int height;
  int width;
  int size;
  int model_size;
  int index_size;
  unsigned int *model_index;
  ColoredVertex *model_data;
  ModelRotateCommand *left_rot;
  ModelRotateCommand *right_rot;
  ModelRotateCommand *stop_left_rot;
  ModelRotateCommand *stop_right_rot;
};