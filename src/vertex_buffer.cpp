#include <GLEW/glew.h>
#include "./vertex_buffer.h"

VertexBuffer *VertexBuffer::current_buffer = nullptr;

VertexBuffer::VertexBuffer()
{
  glGenBuffers(1, &vbo);
}

VertexBuffer::~VertexBuffer()
{
  glDeleteBuffers(1, &vbo);
}

void VertexBuffer::bind()
{
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  current_buffer = this;
}

void VertexBuffer::unbind()
{
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexBuffer::setData(void *data, unsigned int size)
{
  bind();
  glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
  unbind();
}

void VertexBuffer::stateless_bind() {
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
}
